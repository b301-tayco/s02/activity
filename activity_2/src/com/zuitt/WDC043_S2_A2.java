package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args){
        int[] primeNumbers = {2,3,5,7,11};
       System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("----------------");

        ArrayList<String> myList = new ArrayList<String>();
          myList.add("John");
          myList.add("Jane");
          myList.add("Chloe");
          myList.add("Zoey");
        System.out.println("My friends are : " + myList);
        System.out.println("----------------");

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
            inventory.put("toothpaste", 15);
            inventory.put("toothbrush", 20);
            inventory.put("soap", 12);
        System.out.println("Our current inventory consist of: " + inventory);
    }

}
